import React from 'react';
import './App.css';
import NytApp from './components/NytApp';
// import NytResults from './components/NytResults'

export const App = () => {
  return (
    <div className="App">
      <h1 className="title">New York Times</h1>
      <h4 className="title">Search Any Article. Any Time.</h4>
      <NytApp />
      {/* <NytResults /> */}
    </div>
  )
}
